__WARNING__: THIS IS A BETA EDITION AND NOT ALWAYS STABLE, DO NOT TRY IT IN PRODUCTION (YET).

- Source code is hosted on [GitHub](https://github.com/iredmail/dockerized).
  Bug report, feedback, patches are always welcome.
- Base image is [Ubuntu 20.04 (focal)](https://hub.docker.com/_/ubuntu).
- Dockerized iRedMail follows the [Best Practice of iRedMail Easy platform](https://docs.iredmail.org/iredmail-easy.best.practice.html).
- 2 tags are available:
  - `iredmail/mariadb:stable`: Stable version.
  - `iredmail/mariadb:nightly`: Triggered by __EACH__ GitHub commit.

# Quick start

Create a docker environment file used to store custom settings:

```
mkdir /iredmail         # Create a new directory or use any directory
                        # you prefer. `/iredmail/` is just an example
cd /iredmail
touch iredmail-docker.conf

echo HOSTNAME=mail.mydomain.com >> iredmail-docker.conf
echo FIRST_MAIL_DOMAIN=mydomain.com >> iredmail-docker.conf
echo FIRST_MAIL_DOMAIN_ADMIN_PASSWORD=my-secret-password >> iredmail-docker.conf
echo MLMMJADMIN_API_TOKEN=$(openssl rand -base64 32) >> iredmail-docker.conf
echo ROUNDCUBE_DES_KEY=$(openssl rand -base64 24) >> iredmail-docker.conf
```

Create required directories to store application data:

```
cd /iredmail
mkdir -p data/{backup,clamav,custom,imapsieve_copy,mailboxes,mlmmj,mlmmj-archive,mysql,sa_rules,ssl,postfix_queue}
```

Launch the container:

```
docker run \
    --rm \
    --name iredmail \
    --env-file iredmail-docker.conf \
    --hostname mail.mydomain.com \
    -p 80:80 \
    -p 443:443 \
    -p 110:110 \
    -p 995:995 \
    -p 143:143 \
    -p 993:993 \
    -p 25:25 \
    -p 465:465 \
    -p 587:587 \
    -v /iredmail/data/backup:/var/vmail/backup \
    -v /iredmail/data/mailboxes:/var/vmail/vmail1 \
    -v /iredmail/data/mlmmj:/var/vmail/mlmmj \
    -v /iredmail/data/mlmmj-archive:/var/vmail/mlmmj-archive \
    -v /iredmail/data/imapsieve_copy:/var/vmail/imapsieve_copy \
    -v /iredmail/data/custom:/opt/iredmail/custom \
    -v /iredmail/data/ssl:/opt/iredmail/ssl \
    -v /iredmail/data/mysql:/var/lib/mysql \
    -v /iredmail/data/clamav:/var/lib/clamav \
    -v /iredmail/data/sa_rules:/var/lib/spamassassin \
    -v /iredmail/data/postfix_queue:/var/spool/postfix \
    iredmail/mariadb:stable
```

Notes:

- On first run, it will generate a self-signed ssl cert, this may take a long
time, please be patient.
- Each time you run the container, few tasks will be ran:
    - Update SpamAssassin rules.
    - Update ClamAV virus signature database.
- `FIRST_MAIL_DOMAIN_ADMIN_PASSWORD` is only set/reset on first run, not each run.
- All SQL passwords are randomly set/reset by default each time you launch or
  relaunch the container. If you don't like this, please set fixed passwords
  in `iredmail-docker.conf`, e.g. `MYSQL_ROOT_PASSWORD=<your-password>`.
- Do not forget to [setup DNS records](https://docs.iredmail.org/setup.dns.html)
  for your server hostname and email domain names.
- If you're running Docker on Windows and macOS, container will fail to launch
  and you must switch to docker volumes as described below.

If you're running Docker on Windows and macOS, or you just prefer storing
persistent data in Docker volumes, please create required volumes:

```
docker volume create iredmail_backup           # Backup copies
docker volume create iredmail_mailboxes        # All users' mailboxes
docker volume create iredmail_mlmmj            # mailing list data
docker volume create iredmail_mlmmj_archive    # mailing list archive
docker volume create iredmail_imapsieve_copy   # Used by Dovecot plugin 'imapsieve'
docker volume create iredmail_custom           # custom config files
docker volume create iredmail_ssl              # SSL cert/key files
docker volume create iredmail_mysql            # MySQL databases
docker volume create iredmail_clamav           # ClamAV database
docker volume create iredmail_sa_rules         # SpamAssassin rules
docker volume create iredmail_postfix_queue    # Postfix queues
```

Then launch the container with volumes:

```
docker run \
    --rm \
    --name iredmail \
    --env-file iredmail-docker.conf \
    --hostname mail.mydomain.com \
    -p 80:80 \
    -p 443:443 \
    -p 110:110 \
    -p 995:995 \
    -p 143:143 \
    -p 993:993 \
    -p 25:25 \
    -p 465:465 \
    -p 587:587 \
    -v iredmail_backup:/var/vmail/backup \
    -v iredmail_mailboxes:/var/vmail/vmail1 \
    -v iredmail_mlmmj:/var/vmail/mlmmj \
    -v iredmail_mlmmj_archive:/var/vmail/mlmmj-archive \
    -v iredmail_imapsieve_copy:/var/vmail/imapsieve_copy \
    -v iredmail_custom:/opt/iredmail/custom \
    -v iredmail_ssl:/opt/iredmail/ssl \
    -v iredmail_mysql:/var/lib/mysql \
    -v iredmail_clamav:/var/lib/clamav \
    -v iredmail_sa_rules:/var/lib/spamassassin \
    -v iredmail_postfix_queue:/var/spool/postfix \
    iredmail/mariadb:stable
```

# Overview

Only one config file `iredmail-docker.conf` on Docker host.

This file is optional if you prefer overwriting parameters with the `-e`
argument while launching container. For example:

```
docker run -e HOSTNAME=mail.mydomain.com -e FIRST_MAIL_DOMAIN=mydomain.com ...
```

We recommend storing them in an env file (`iredmail-docker.conf` in our
example) to save some typing each time you launch the container.

# Required parameters

There're few __REQUIRED__ parameters you __MUST__ set in `iredmail-docker.conf`:

```
# Server hostname. Must be a FQDN. For example, mail.mydomain.com
HOSTNAME=

# First mail domain name. For example, mydomain.com.
FIRST_MAIL_DOMAIN=

# (Plain) password of mail user `postmaster@<FIRST_MAIL_DOMAIN>`.
FIRST_MAIL_DOMAIN_ADMIN_PASSWORD=

# A secret token used for accessing mlmmjadmin API.
MLMMJADMIN_API_TOKEN=

# The secret string used to encrypt/decrypt Roundcube session data.
# Required if you need to run Roundcube webmail.
# You can generate random string with command `openssl rand -base64 24` as the
# des key.
# Every time this key changed, all Roundcube session data becomes invalid and
# users will be forced to re-login.
ROUNDCUBE_DES_KEY=
```

Notes:

- `iredmail-docker.conf` will be read by Docker as an environment file,
  any single quote or double quote will be treated as part of the value.
  __Do not use any whitespace, tab in value, and no single or double quotes.__
- It will be imported as bash shell script too.

There're many OPTIONAL settings defined in file
`/docker/entrypoints/settings.conf` inside docker container,
you'd like to change any of them, please write the same parameter name with
your custom value in `iredmail-docker.conf` to override it.

# Optional parameters

```
# Define your custom https port if you don't want to the default one (443).
PORT_HTTPS=4443
```

# Hardware requirements

- At least 4GB RAM is required for a low traffic production mail server.

# Installed softwares

- Postfix: SMTP server.
- Dovecot: POP3/IMAP/LMTP/Sieve server, also offers SASL AUTH service for Postfix.
- mlmmj: mailing list manager.
- mlmmjadmin: RESTful API server used to manage (mlmmj) mailing lists.
- Amavisd-new + ClamAV + SpamAssassin: anti-spam and anti-virus, DKIM signing and verification, etc.
- iRedAPD: Postfix policy server. Developed by iRedMail team.
- Fail2ban: scans log files and bans bad clients.
- Roundcube: webmail.
- iRedAdmin: web-based admin panel, open source edition.

You may want to check [this tutorial](https://docs.iredmail.org/network.ports.html)
to figure out the mapping of softwares and network ports.

# Exposed network ports

- 80: HTTP
- 443: HTTPS
- 25: SMTP
- 465: SMTPS (SMTP over SSL)
- 587: SUBMISSION (SMTP over TLS)
- 143: IMAP over TLS
- 993: IMAP over SSL
- 110: POP3 over TLS
- 995: POP3 over SSL
- 4190: Managesieve service

# Links

- If you're trying to deploy this docker image with Ansible, user @ricristian
  already have some code for your reference: <https://github.com/ricristian/ansible-dockerized-iredmail>
||||||| (empty tree)
=======
# iredmail_docker



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/wt0f/iredmail_docker.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://docs.gitlab.com/ee/user/application_security/sast/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:c7135f27f4b2a40b0a6662fe9762ae7e?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

